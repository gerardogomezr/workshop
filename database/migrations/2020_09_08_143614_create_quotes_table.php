<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('coupon_code_id')
                ->nullable();
            $table->enum('status', [
                'ACTIVE', // Cuando aún se está componiendo
                'SENT', // Cuando se haya enviado
                'CONVERTED', // Cuando se haya convertido en compra
            ]);
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('coupon_code_id')
                ->references('id')
                ->on('coupon_codes')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
