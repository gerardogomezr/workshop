<?php

// User related routes
$router->post('register', 'RegisterController@register');
$router->get('me', 'AccountController@me');

// API resources routes
$router->apiResource('services', 'ServiceController');
$router->apiResource('coupon-codes', 'CouponCodeController');
