<?php

namespace App\Http\Controllers;

use App\Http\Requests\Services\StoreRequest;
use App\Http\Requests\Services\UpdateRequest;
use App\Http\Resources\ServiceResource;
use App\Service;

class ServiceController extends Controller
{
    /**
     * Service model
     *
     * @var Service
     */
    protected $service;

    /**
     * New controller instance
     *
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ServiceResource::collection(
            $this->service->newQuery()
                ->paginate(10)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $service = $this->service
            ->create($request->validated());

        return ServiceResource::make($service);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service              $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        return ServiceResource::make($service);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service              $service
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Service $service)
    {
        $service->fill($request->validated());
        $service->save();
        return ServiceResource::make($service);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service              $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $service->delete();
        return ServiceResource::make($service);
    }
}
