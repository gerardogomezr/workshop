<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use Illuminate\Contracts\Auth\Factory;

class AccountController extends Controller
{
    /**
     * Shows the current user
     *
     * @param  Factory      $auth
     * @return UserResource
     */
    public function me(Factory $auth)
    {
        $user = $auth->guard('api')->user();
        return UserResource::make($user);
    }
}
