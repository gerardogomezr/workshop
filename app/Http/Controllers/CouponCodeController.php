<?php

namespace App\Http\Controllers;

use App\Http\Requests\CouponCodes\StoreRequest;
use App\Http\Requests\CouponCodes\UpdateRequest;
use App\Http\Resources\CouponCodeResource;
use App\CouponCode;

class CouponCodeController extends Controller
{
    /**
     * CouponCode model
     *
     * @var CouponCode
     */
    protected $coupon_code;

    /**
     * New controller instance
     *
     * @param CouponCode $coupon_code
     */
    public function __construct(CouponCode $coupon_code)
    {
        $this->coupon_code = $coupon_code;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CouponCodeResource::collection(
            $this->coupon_code->newQuery()
                ->paginate(10)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $coupon_code = $this->coupon_code
            ->create($request->validated());

        return CouponCodeResource::make($coupon_code);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CouponCode           $coupon_code
     * @return \Illuminate\Http\Response
     */
    public function show(CouponCode $coupon_code)
    {
        return CouponCodeResource::make($coupon_code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CouponCode           $coupon_code
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, CouponCode $coupon_code)
    {
        $coupon_code->fill($request->validated());
        $coupon_code->save();
        return CouponCodeResource::make($coupon_code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CouponCode           $coupon_code
     * @return \Illuminate\Http\Response
     */
    public function destroy(CouponCode $coupon_code)
    {
        $coupon_code->delete();
        return CouponCodeResource::make($coupon_code);
    }
}
