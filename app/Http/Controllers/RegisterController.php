<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Contracts\Hashing\Hasher;

class RegisterController extends Controller
{
    /**
     * User model
     *
     * @var User
     */
    protected $users;

    /**
     * Controller constructor
     *
     * @param User $users
     */
    public function __construct(User $users)
    {
        $this->users = $users;
    }

    /**
     * Register a user
     *
     * @param  RegisterRequest $request
     * @return UserResource
     */
    public function register(
        Hasher $hasher,
        RegisterRequest $request
    ) {
        $user = $this->users->newInstance();
        $user->fill($request->except('password'));
        $user->password = $hasher->make($request->get('password'));
        $user->save();

        return UserResource::make($user);
    }
}
