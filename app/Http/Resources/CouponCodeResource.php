<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CouponCodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'max_uses'   => $this->max_uses,
            'code'       => $this->code,
            'discount'   => $this->discount,
            'created_at' => $this->created_at,
        ];
    }
}
