<?php

namespace App\Http\Requests\CouponCodes;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'     => ['required', 'string'],
            'max_uses' => ['required', 'numeric', 'min:1'],
            'discount' => ['required', 'numeric', 'min:1'],
        ];
    }
}
