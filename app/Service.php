<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Service extends Model
{
    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['name', 'price'];

    /** @return BelongsToMany  */
    public function quotes(): BelongsToMany
    {
        return $this->belongsToMany(Quote::class)
            ->withPivot('quantity');
    }
}
