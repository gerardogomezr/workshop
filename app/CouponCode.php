<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CouponCode extends Model
{
    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['max_uses', 'code', 'discount'];

    /** @return HasMany  */
    public function quotes(): HasMany
    {
        return $this->hasMany(Quote::class);
    }
}
